package ru.mtumanov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("ERROR! Task not found!");
    }

}
