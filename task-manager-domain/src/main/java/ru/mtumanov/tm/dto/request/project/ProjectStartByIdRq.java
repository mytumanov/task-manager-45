package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public ProjectStartByIdRq(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
