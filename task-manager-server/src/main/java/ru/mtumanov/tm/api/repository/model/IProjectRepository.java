package ru.mtumanov.tm.api.repository.model;

import ru.mtumanov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
