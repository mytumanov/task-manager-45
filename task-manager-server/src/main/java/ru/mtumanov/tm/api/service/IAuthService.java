package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.exception.AbstractException;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout(@Nullable SessionDTO session) throws AbstractException;

    @NotNull
    SessionDTO validateToken(@Nullable String token) throws AbstractException;

}
