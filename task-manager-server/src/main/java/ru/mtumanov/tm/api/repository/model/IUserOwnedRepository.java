package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull M entity) throws AbstractException;

    void clear(@NotNull String userId);

    @NotNull
    M update(@NotNull M entity) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull String userId, @NotNull String id);

}
