package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskDtoService implements IDtoProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected IDtoTaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    protected IDtoProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository(entityManager).existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = getTaskRepository(entityManager).findOneById(userId, taskId);
        try {
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository(entityManager).existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final List<TaskDTO> tasks = getTaskRepository(entityManager).findAllByProjectId(userId, projectId);
        try {
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks)
                getTaskRepository(entityManager).removeById(task.getId());
            getProjectRepository(entityManager).removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository(entityManager).existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = getTaskRepository(entityManager).findOneById(userId, taskId);
        task.setProjectId(null);
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
