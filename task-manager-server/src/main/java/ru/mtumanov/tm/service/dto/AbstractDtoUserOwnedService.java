package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoUserOwnedRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoUserOwnedService;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IDtoUserOwnedRepository<M>>
        extends AbstractDtoService<M, R> implements IDtoUserOwnedService<M> {

    protected AbstractDtoUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        model.setUserId(userId);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return getRepository(connectionService.getEntityManager()).existById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return getRepository(connectionService.getEntityManager()).findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return getRepository(connectionService.getEntityManager()).findAll(userId);
        return getRepository(connectionService.getEntityManager()).findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return getRepository(connectionService.getEntityManager()).findOneById(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return getRepository(connectionService.getEntityManager()).getSize(userId);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).removeById(userId, model.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
