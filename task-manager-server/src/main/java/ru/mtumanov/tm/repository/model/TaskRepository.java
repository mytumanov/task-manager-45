package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    public TaskRepository(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks)
            remove(task);
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId", Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator), Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p WHERE p.userId = :userId AND p.id = :id", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Task project = findOneById(userId, id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks)
            remove(task);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p", Task.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Task> findAll(@NotNull final Comparator<Task> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task p ORDER BY p." + getComporator(comparator), Task.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.find(Task.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final Task project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;

    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM Task t WHERE userId = :userId AND projectId = :projectId", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
